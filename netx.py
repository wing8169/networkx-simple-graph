import networkx as nx   # networkx is used to model graph
import pylab as plt     # plt is used to draw and save graph
import csv              # csv is comma-separated file
import codecs           # codecs is used to get chinese characters from csv


# convert csv data to nodes, edges and labels
def csv_to_data():
    nodes = dict()
    edges = []
    labels = dict()

    with codecs.open("data.csv", "r", "utf-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        i = 0
        is_nodes = True
        for row in csv_reader:
            if row == [] or row[0] == "":
                continue
            if row[0] == "nodes":
                is_nodes = True
                continue
            if row[0] == "edges":
                is_nodes = False
                continue
            if is_nodes:
                nodes[i] = (int(row[0]), int(row[1]))
                labels[i] = row[2]
            else:
                edges.append((int(row[0]), int(row[1]), int(row[2])))
            i += 1
        return nodes, edges, labels


def main():
    nodes, edges, labels = csv_to_data()
    G = nx.Graph()
    G.add_nodes_from(nodes.keys())
    G.add_weighted_edges_from(edges)

    # assign 'pos' value to each node in case if need to use
    for n, p in nodes.items():
        G.nodes[n]['pos'] = p

    weights = nx.get_edge_attributes(G, 'weight')

    # draw the graph, if need change design most probably will change the lines below
    nx.draw(G, labels=labels, pos=nodes, with_labels=True, font_family='SimHei', node_color='white')
    nx.draw_networkx_edge_labels(G, nodes, edge_labels=weights)
    plt.margins(0.1, 0.1)

    # either show on window pop up or savefig, can only choose one option.
    plt.show()
    # plt.savefig('graph.png')


if __name__ == "__main__":
    main()
